import {weatherInfo} from './apiDataModelling.js';
import {currentWeatherTemplate} from './htmlTemplates.js';

const input = document.querySelector("#input");
const searchBtn = document.querySelector(".search-btn");
const currentDay = document.querySelector(".current-day");

let renderCurrentDayDiv = function(location){
    return weatherInfo(location)
    .then(weather => {
        currentWeatherTemplate(weather);
    })
};

const searchPlace = function(){
    if(typeof input.value !== "string" ){return}
    else if(input.value === ""){return}
    else { 
        currentDay.innerHTML = "";
        renderCurrentDayDiv(input.value)
        input.value = "";
    }
}

searchBtn.addEventListener('click', searchPlace);
window.addEventListener('keyup', (e) => {
    if(e.keycode === "Enter"){
        searchPlace;
    }
});

window.addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
        searchPlace();
    }
});

renderCurrentDayDiv('Cordoba');